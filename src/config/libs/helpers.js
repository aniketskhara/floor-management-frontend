const buildHeader = () => {
  return {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '.concat(localStorage.getItem('jwt-token'))
  }
};

export {
  buildHeader,
}