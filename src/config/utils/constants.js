const appContants = {
  redux: {},
  routes: [
    {
      route: "/admin/dashboard",
      title: "Dashboard",
      icon: "las la-database",
      value: "dashboard"
    },
    {
      route: "/admin/role-management",
      title: "Role Management",
      icon: "glyphicon glyphicon-registration-mark",
      value: "role-management"
    },
    {
      route: "/admin/device-manager",
      title: "Device Manager",
      icon: "glyphicon glyphicon-phone",
      value: "device-manager"
    },
    {
      route: "/admin/cabin-manager",
      title: "Cabin Manager",
      icon: "la la-th-large",
      value: "cabin-manager"
    },
    {
      route: "/admin/employee",
      title: "Employee",
      icon: "las la-users",
      value: "employee"
    },
    {
      route: "/admin/master",
      title: "Masters",
      icon: "las la-user-secret",
      value: "master"
    }
  ],
  master_routes: [
    {
      route: "/admin/master/cabins",
      title: "Cabins"
    },
    {
      route: "/admin/master/roles",
      title: "Roles"
    },
    {
      route: "/admin/master/devises",
      title: "Devises"
    },
    {
      route: "/admin/master/departments",
      title: "Departments"
    },
    {
      route: "/admin/master/technologies",
      title: "Technologies"
    }
  ]
};

export default appContants;
