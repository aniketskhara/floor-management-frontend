import React, { Component } from "react";
import axios from "axios";
import NotificationAlert from "react-notification-alert";
import { withRouter, Redirect } from "react-router-dom";
import Loader from "react-loader-spinner";
import "./main.css";
import "./util.css";
import "./vendor/bootstrap/css/bootstrap.min.css";
import "./fonts/font-awesome-4.7.0/css/font-awesome.min.css";
import "./fonts/Linearicons-Free-v1.0.0/icon-font.min.css";
import "./vendor/animate/animate.css";
import "./vendor/css-hamburgers/hamburgers.min.css";
import "./vendor/css-hamburgers/hamburgers.min.css";
import "./vendor/animsition/css/animsition.min.css";
import "./vendor/select2/select2.min.css";
import "./vendor/daterangepicker/daterangepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import "./vendor/overlayStyle.css";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      pass: "",
      isLoggedIn: false,
      loading: false,
      isDisable: false,
      LoginFormData: true
    };
  }

  componentDidMount() {}

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notifymsg) {
      this.refs.notifymsg.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  loginForm = async e => {
    const { history } = this.props;
    this.setState({ LoginFormData: false });
    e.preventDefault();
    this.setState({ loading: true });

    try {
      await axios
        .post(`https://shrouded-springs-70350.herokuapp.com/employees/login`, {
          email: this.state.username,
          password: this.state.pass
        })
        .then(async res => {
          this.setState({ loading: false });
          localStorage.setItem("user_id", res.data.user_id);
          localStorage.setItem("jwt-token", res.data.token);
          localStorage.setItem("user-email", res.data.result.email);
          await this.getPermissions(res.data.result.role);
          this.NotificationHandler("Login Successfully", "success");
          history.push("/admin");
        });
    } catch (e) {
      this.setState({ LoginFormData: true });
      this.NotificationHandler("Unable to login", "danger");
    }
  };

  getPermissions = async role => {
    this.setState({ loading: true });
    return new Promise(async resolve => {
      try {
        await axios
          .get(
            `https://shrouded-springs-70350.herokuapp.com/role-management/role-item/${role._id}`,
            {
              headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer ".concat(
                  localStorage.getItem("jwt-token")
                )
              }
            }
          )
          .then(res => {
            this.setState({ loading: false });
            localStorage.setItem("access", res.data.RolesWithAccess.access);
            resolve();
          });
      } catch (e) {
        console.log(e);
      }
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  showCurrentPassword = (id) => {
    var x = document.getElementById(id);
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  };

  render() {
    let LoginFormData = this.state.LoginFormData ? (
      <div>
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100 p-t-30 p-b-50">
              <span className="login100-form-title p-b-41">Account Login</span>
              <form
                className="login100-form validate-form p-b-33 p-t-5"
                action="#"
                onSubmit={this.submitHandler}
              >
                <div
                  className="wrap-input100 validate-input username"
                  data-validate="Enter username"
                >
                  <input
                    className="input100"
                    type="text"
                    name="username"
                    placeholder="User name"
                    onChange={this.handleChange}
                  />
                  <span
                    className="focus-input100"
                    data-placeholder="&#xe82a;"
                  ></span>
                </div>

                <div
                  className="wrap-input100 validate-input password"
                  data-validate="Enter password"
                >
                  <input
                    className="input100"
                    type="password"
                    name="pass"
                    placeholder="Password"
                    id="pwd"
                    onChange={this.handleChange}
                  />
                  <span
                    className="focus-input100"
                    data-placeholder="&#xe80f;"
                  ></span>
                  <span class="p-viewer custom-viewer" onClick={() => this.showCurrentPassword("pwd")}>
                    <i class="fa fa-eye" aria-hidden="true" ></i>
                  </span>
                </div>

                <div className="container-login100-form-btn m-t-32 login-button-div">
                  <button
                    className="login100-form-btn"
                    onClick={this.loginForm}
                  >
                    Login
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    ) : (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
            <h1>Logging In</h1>
          </div>
        </div>
      </div>
    );

    return (
      <React.Fragment>
        <div>
          <NotificationAlert ref="notifymsg" />
        </div>
        {LoginFormData}
      </React.Fragment>
    );
  }
}

export default withRouter(Home);
