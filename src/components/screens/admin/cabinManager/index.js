import React, { Component } from "react";
import HTML5Backend from "react-dnd-html5-backend";
import Scheduler, {
  SchedulerData,
  ViewTypes,
  DATE_FORMAT,
  AddMorePopover
} from "react-big-scheduler";
import "react-big-scheduler/lib/css/style.css";
import { PropTypes } from "prop-types";
import Modal from "react-modal";
import moment from "moment";
import "./calendar.css";
import withDragDropContext from "./withDnDContext";
import axios from "axios";
import { helpers } from "../../../../config/index";
import DateTimePicker from "react-datetime-picker";
import NotificationAlert from "react-notification-alert";
import Loader from "react-loader-spinner";
import "../../home/vendor/overlayStyle.css";

const headers = {};

const customStyles = {
  content: {
    top: "10%",
    // left: "54%",
    // right: "auto",
    // bottom: "auto",
    // marginRight: "-50%",
    // transform: "translate(-50%, -50%)",
    margin: "auto",
    width: "40%",
    height: "300px"
  }
};

class CabinManager extends Component {
  constructor(props) {
    super(props);
    let schedulerData = new SchedulerData(
      new Date(),
      ViewTypes.Week,
      false,
      false,
      {
        checkConflict: true
      }
    );
    schedulerData.localeMoment.locale("en");
    this.state = {
      viewModel: schedulerData,
      headerItem: undefined,
      left: 0,
      top: 0,
      modalIsOpen: false,
      title: "",
      start: new Date(),
      end: new Date(),
      resourceId: "",
      user_id: "",
      height: 0,
      cabin: "",
      cabins: [],
      cabinList: [],
      cabinBookings: [],
      loading: false
    };
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    schedulerData.config.resourceName = "Cabins";
    schedulerData.config.schedulerWidth = "80%";
  }

  setSchedularData = cabins => {
    let schedulerData = this.state.viewModel;
    let newCabins = [];
    let len = cabins.length;
    for (var i = 0; i < len; i++) {
      newCabins.push({
        id: cabins[i]._id,
        name: cabins[i].cabin_no
      });
    }
    schedulerData.setResources(newCabins);
    this.setState({ viewModel: schedulerData });
  };

  setCabinBookings = bookings => {
    let schedulerData = this.state.viewModel;
    let newCabinBookings = [];
    let len = bookings.length;
    for (var i = 0; i < len; i++) {
      newCabinBookings.push({
        id: bookings[i]._id,
        resourceId: bookings[i].resourceId,
        start: bookings[i].start,
        end: bookings[i].end,
        title: bookings[i].title
      });
    }
    schedulerData.setEvents(newCabinBookings);
    this.setState({ cabinBookings: newCabinBookings });
    this.setState({ viewModel: schedulerData });
  };

  componentDidMount() {
    this.getAllCabins();
    this.getCabinBooking();
  }

  clearstate = () => {
    this.setState({
      title: "",
      resourceId: "",
      user_id: "",
      start: new Date(),
      end: new Date()
    });
  };

  getCabinBooking = async () => {
    this.setState({ loading: true });
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/cabin-management", {
        headers: helpers.buildHeader()
      })
      .then(res => {
        this.setState({
          cabinBookings: res.data.CabinWithAccess,
          loading: false
        });
        // this.setSchedularData(res.data.cabins);
        this.setCabinBookings(res.data.CabinWithAccess);
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch cabins", "danger");
      });
  };

  getAllCabins = () => {
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/cabins", {
        headers: helpers.buildHeader()
      })
      .then(res => {
        this.setState({ cabins: res.data.cabins, cabinList: res.data.cabins });
        this.setSchedularData(res.data.cabins);
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch cabins", "danger");
      });
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 5
      });
    }
  };

  prevClick = schedulerData => {
    schedulerData.prev();
    schedulerData.setEvents(this.state.cabinBookings);
    this.setState({
      viewModel: schedulerData
    });
  };

  nextClick = schedulerData => {
    schedulerData.next();
    schedulerData.setEvents(this.state.cabinBookings);
    this.setState({
      viewModel: schedulerData
    });
  };

  onViewChange = (schedulerData, view) => {
    schedulerData.setViewType(
      view.viewType,
      view.showAgenda,
      view.isEventPerspective
    );
    schedulerData.setEvents(this.state.cabinBookings);
    this.setState({
      viewModel: schedulerData
    });
  };

  onSelectDate = (schedulerData, date) => {
    schedulerData.setDate(date);
    schedulerData.setEvents(this.state.cabinBookings);
    this.setState({
      viewModel: schedulerData
    });
  };

  eventClicked = (schedulerData, event) => {
    alert(
      `You just clicked an event: {id: ${event.id}, title: ${event.title}}`
    );
  };

  newEvent = (schedulerData, slotId, slotName, start, end, type, item) => {
    let newEvent = {
      id: item._id,
      title: slotName,
      start: start,
      end: end,
      resourceId: slotId,
      bgColor: "purple"
    };
    schedulerData.addEvent(newEvent);
    this.setState({
      viewModel: schedulerData
    });
  };

  updateEventStart = (schedulerData, event, newStart) => {
    if (
      window.confirm(
        `Do you want to adjust the start of the event? {eventId: ${event.id}, eventTitle: ${event.title}, newStart: ${newStart}}`
      )
    ) {
      schedulerData.updateEventStart(event, newStart);
    }
    this.setState({
      viewModel: schedulerData
    });
  };

  updateEventEnd = (schedulerData, event, newEnd) => {
    if (
      window.confirm(
        `Do you want to adjust the end of the event? {eventId: ${event.id}, eventTitle: ${event.title}, newEnd: ${newEnd}}`
      )
    ) {
      schedulerData.updateEventEnd(event, newEnd);
    }
    this.setState({
      viewModel: schedulerData
    });
  };

  moveEvent = (schedulerData, event, slotId, slotName, start, end) => {
    if (
      window.confirm(
        `Do you want to move the event? {eventId: ${event.id}, eventTitle: ${event.title}, newSlotId: ${slotId}, newSlotName: ${slotName}, newStart: ${start}, newEnd: ${end}`
      )
    ) {
      schedulerData.moveEvent(event, slotId, slotName, start, end);
      this.setState({
        viewModel: schedulerData
      });
    }
  };

  onScrollRight = (schedulerData, schedulerContent, maxScrollLeft) => {
    if (schedulerData.ViewTypes === ViewTypes.Day) {
      schedulerData.next();
      schedulerData.setEvents(this.state.cabinBookings);
      this.setState({
        viewModel: schedulerData
      });

      schedulerContent.scrollLeft = maxScrollLeft - 10;
    }
  };

  onScrollLeft = (schedulerData, schedulerContent, maxScrollLeft) => {
    if (schedulerData.ViewTypes === ViewTypes.Day) {
      schedulerData.prev();
      schedulerData.setEvents(this.state.cabinBookings);
      this.setState({
        viewModel: schedulerData
      });

      schedulerContent.scrollLeft = 10;
    }
  };

  onScrollTop = (schedulerData, schedulerContent, maxScrollTop) => {
    console.log("onScrollTop");
  };

  onScrollBottom = (schedulerData, schedulerContent, maxScrollTop) => {
    console.log("onScrollBottom");
  };

  onSetAddMoreState = newState => {
    if (newState === undefined) {
      this.setState({
        headerItem: undefined,
        left: 0,
        top: 0,
        height: 0
      });
    } else {
      this.setState({
        ...newState
      });
    }
  };

  toggleExpandFunc = (schedulerData, slotId) => {
    schedulerData.toggleExpandStatus(slotId);
    this.setState({
      viewModel: schedulerData
    });
  };

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = "#f00";
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  onStartChange = date => {
    this.setState({ start: date });
  };

  onEndChange = date => {
    this.setState({ end: date });
  };

  Options = ({ options }) => {
    return options.map(option => (
      <option key={option._id} value={option._id}>
        {option.cabin_no}
      </option>
    ));
  };

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  bookCabin = async () => {
    let current_datetime = new Date();
    let startDateTime = moment(new Date(this.state.start));
    let endDateTime = moment(new Date(this.state.end));

    const existing_bookings = this.state.cabinBookings.find(
      existing_booking =>
        new Date(existing_booking.start).getTime() ===
          this.state.start.getTime() &&
        existing_booking.resourceId === this.state.cabin
    );

    let existingBetween = this.state.cabinBookings.filter(
      existingBook =>
        (startDateTime.isBetween(
          moment(new Date(existingBook.start)),
          moment(new Date(existingBook.end))
        ) ||
          endDateTime.isBetween(
            moment(new Date(existingBook.start)),
            moment(new Date(existingBook.end))
          )) &&
        existingBook.resourceId === this.state.cabin
    );
    console.log(existingBetween);

    if (
      existing_bookings ||
      existingBetween.length >= 1 ||
      startDateTime < moment(new Date())
    ) {
      console.log(
        existing_bookings ||
          existingBetween ||
          startDateTime < moment(new Date()) ||
          endDateTime < moment(new Date())
      );
      this.NotificationHandler(
        "Please select the valid date and time or check if cabin is already booked",
        "danger"
      );
    } else {
      try {
        await axios
          .post(
            `https://shrouded-springs-70350.herokuapp.com/cabin-management/`,
            {
              title: this.state.title,
              resourceId: this.state.cabin,
              start: this.state.start,
              end: this.state.end,
              employee_id: localStorage.getItem("user_id")
            },
            { headers: helpers.buildHeader() }
          )
          .then(res => {
            this.closeModal();
            this.NotificationHandler("Cabin booked successfully", "success");
            this.newEvent(
              this.state.viewModel,
              res.data.CabinWithAccess.resourceId,
              res.data.CabinWithAccess.title,
              res.data.CabinWithAccess.start,
              res.data.CabinWithAccess.end,
              null,
              res.data.CabinWithAccess
            );
            this.clearstate();
            this.getAllCabins();
            this.getCabinBooking();
          });
      } catch (err) {
        this.NotificationHandler("Unable to book cabin", "danger");
      }
    }
  };

  render() {
    const { viewModel } = this.state;
    let popover = <div />;

    if (this.state.headerItem !== undefined) {
      popover = (
        <AddMorePopover
          headerItem={this.state.headerItem}
          schedulerData={viewModel}
          closeAction={this.onSetAddMoreState}
          left={this.state.left}
          top={this.state.top}
          height={this.state.height}
        />
      );
    }

    let CalendarData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="row">
        <div className="col-md-12 text-right">
          <button onClick={this.openModal} className="btn btn-dark">
            <i className="fa fa-plus" title="Add Cabin"></i>
          </button>
        </div>
      </div>
    );
    return (
      <div>
        <div>
          <NotificationAlert ref="notify" />
          {CalendarData}
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Example Modal"
          >
            <button
              onClick={this.closeModal}
              type="button"
              className="close"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 ref={subtitle => (this.subtitle = subtitle)}>Book Cabin</h3>
            <hr />
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="Enter title"
                    name="title"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                </div>
                <div className="col-md-6">
                  <select
                    className="form-control"
                    onChange={this.handleChange}
                    name="cabin"
                    defaultValue="---Select Cabin---"
                  >
                    <option
                      disabled={true}
                      selected="selected"
                      value="---Select Cabin---"
                    >
                      {"---Select Cabin---"}
                    </option>
                    <this.Options options={this.state.cabinList} />
                  </select>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <DateTimePicker
                    onChange={this.onStartChange}
                    value={this.state.start}
                  />
                </div>
                <div className="col-md-6">
                  <DateTimePicker
                    onChange={this.onEndChange}
                    value={this.state.end}
                  />
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-md-12 text-center">
                  <button onClick={this.bookCabin} className="btn btn-dark">
                    Book
                  </button>
                </div>
              </div>
            </form>
          </Modal>

          <Scheduler
            schedulerData={viewModel}
            prevClick={this.prevClick}
            nextClick={this.nextClick}
            onSelectDate={this.onSelectDate}
            onViewChange={this.onViewChange}
            updateEventStart={this.updateEventStart}
            updateEventEnd={this.updateEventEnd}
            onSetAddMoreState={this.onSetAddMoreState}
            onScrollLeft={this.onScrollLeft}
            onScrollRight={this.onScrollRight}
            onScrollTop={this.onScrollTop}
            onScrollBottom={this.onScrollBottom}
            toggleExpandFunc={this.toggleExpandFunc}
          />
          {popover}
        </div>
      </div>
    );
  }
}

// export default CabinManager;
export default withDragDropContext(CabinManager);
