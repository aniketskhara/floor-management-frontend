import React, { Component } from "react";
import axios from "axios";
import Modal from "react-modal";
import NotificationAlert from "react-notification-alert";
import { helpers } from "../../../../config";
import "./cabin.css";
import Loader from "react-loader-spinner";
import "../../home/vendor/overlayStyle.css";

const headers = {};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "35%"
  }
};

Modal.setAppElement("*");

class Department extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departments: [],
      modalIsOpen: false,
      newModalIsOpen: false,
      department: "",
      updated_department: "",
      department_id: "",
      departmentsCount: 0,
      loading: false
    };
  }

  componentDidMount() {
    this.getAllDepartments();
  }

  getAllDepartments = () => {
    this.setState({ loading: true });
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/departments", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({
          departments: response.data.departments,
          departmentsCount: response.data.departmentsCount,
          loading: false
        });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch department", "danger");
      });
  };

  openModal = item => {
    this.setState({
      modalIsOpen: true
    });
    this.setItem(item);
  };

  openNewModal = () => {
    this.setState({
      newModalIsOpen: true
    });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, newModalIsOpen: false });
  };

  // On opening the modal set department id and no in props
  setItem(item) {
    this.setState({
      department_id: item._id,
      department: item.name
    });
  }

  handleChange = e => {
    this.setState({
      updated_department: e.target.value
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  // Update the departments
  updateDepartment = async () => {
    let name = this.state.updated_department
    
    if (!/^[a-zA-Z]+$/.test(name)){
      this.NotificationHandler("Please enter valid department name", "danger");
    }
    else {
      try {
        await axios
          .patch(
            `https://shrouded-springs-70350.herokuapp.com/departments/${this.state.department_id}`,
            {
              name: this.state.updated_department
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.closeModal();
            this.NotificationHandler(
              "Department updated successfully",
              "success"
            );
            this.getAllDepartments();
          });
      } catch (e) {
        this.NotificationHandler("Unable to update department", "danger");
      }
    }
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };
  // Adding new department
  addDepartment = async () => {
    let name = this.state.updated_department
    
    if (!/^[a-zA-Z]+$/.test(name)){
      this.NotificationHandler("Please enter valid department name", "danger");
    }
    else {
      try {
        await axios
          .post(
            `https://shrouded-springs-70350.herokuapp.com/departments/`,
            {
              name: this.state.updated_department
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.closeModal();
            this.NotificationHandler("Department added successfully", "success");
            this.getAllDepartments();
          });
      } catch (e) {
        this.NotificationHandler("Unable to add department", "danger");
      }
    }
  };

  // Remove departments
  removeDepartment = async id => {
    try {
      await axios
        .delete(
          `https://shrouded-springs-70350.herokuapp.com/departments/${id}`,
          { headers: helpers.buildHeader() }
        )
        .then(() => {
          this.closeModal();
          this.NotificationHandler(
            "Department removed successfully",
            "success"
          );
          this.getAllDepartments();
        });
    } catch (e) {
      this.NotificationHandler("Unable to remove department", "danger");
    }
  };

  render() {
    const { departments } = this.state;
    let FormData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="app-page">
        <span className="counts">
          Total Departments: {this.state.departmentsCount}
        </span>
        <button
          type="button"
          className="btn btn-dark add-cabin"
          onClick={this.openNewModal.bind(this)}
        >
          <i className="fa fa-plus" title="Add Department"></i>
        </button>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Sr.No</th>
              <th scope="col">Department</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {departments.map((item, index) => (
              <tr className={item.department} key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.openModal.bind(this, item)}
                  >
                    <i className="fa fa-pencil" title="Edit"></i>
                  </button>
                </td>
                <td>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.removeDepartment.bind(this, item._id)}
                  >
                    <i className="fa fa-trash" title="Remove"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        {FormData}
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Edit Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>Edit Department</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    defaultValue={this.state.department}
                    onChange={this.handleChange}
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary update-cabin"
                    onClick={this.updateDepartment.bind(this)}
                  >
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
        <Modal
          isOpen={this.state.newModalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="New Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>New Department</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    onChange={this.handleChange}
                    placeholder="Enter department name"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary update-cabin"
                    onClick={this.addDepartment.bind(this)}
                  >
                    New Department
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Department;
