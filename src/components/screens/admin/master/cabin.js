import React, { Component } from "react";
import axios from "axios";
import Modal from "react-modal";
import NotificationAlert from "react-notification-alert";
import { helpers } from "../../../../config";
import Loader from "react-loader-spinner";
import "./cabin.css";
import "../../home/vendor/overlayStyle.css";

const headers = {};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "35%"
  }
};

class Cabin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cabins: [],
      modalIsOpen: false,
      newModalIsOpen: false,
      cabin_no: "",
      cabin_id: "",
      cabinsCount: 0,
      loading: false
    };
  }

  componentDidMount() {
    this.getAllCabins();
  }

  getAllCabins = () => {
    this.setState({ loading: true });
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/cabins", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({
          cabins: response.data.cabins,
          cabinsCount: response.data.cabinsCount,
          loading: false
        });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch cabins", "danger");
      });
  };

  openModal = item => {
    this.setState({
      modalIsOpen: true
    });
    this.setItem(item);
  };

  openNewModal = () => {
    this.setState({
      newModalIsOpen: true
    });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, newModalIsOpen: false });
  };

  // On opening the modal set cabin id and no in props
  setItem(item) {
    this.setState({
      cabin_id: item._id,
      cabin_no: item.cabin_no
    });
  }

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  clearStates = () => {
    this.setState({
      cabin_no: ""
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  // Update the cabin no
  updateCabinNo = async e => {
    e.preventDefault();
    try {
      await axios
        .patch(
          `https://shrouded-springs-70350.herokuapp.com/cabins/${this.state.cabin_id}`,
          {
            cabin_no: this.state.cabin_no
          },
          { headers: helpers.buildHeader() }
        )
        .then(() => {
          this.closeModal();
          this.NotificationHandler("Cabin updated successfully", "success");
          this.clearStates();
          this.getAllCabins();
        });
    } catch (e) {
      this.NotificationHandler("Unable to update the cabin", "danger");
    }
  };

  // Adding new cabin
  addCabinNo = async () => {
    try {
      await axios
        .post(
          `https://shrouded-springs-70350.herokuapp.com/cabins/`,
          {
            cabin_no: this.state.cabin_no
          },
          { headers: helpers.buildHeader() }
        )
        .then(() => {
          this.closeModal();
          this.NotificationHandler("Cabin added successfully", "success");
          this.clearStates();
          this.getAllCabins();
        });
    } catch (e) {
      this.NotificationHandler("Unable to add cabin", "danger");
    }
  };

  // Remove cabin
  removeCabin = async id => {
    try {
      await axios
        .delete(`https://shrouded-springs-70350.herokuapp.com/cabins/${id}`, {
          headers: helpers.buildHeader()
        })
        .then(() => {
          this.closeModal();
          this.NotificationHandler("Cabin removed successfully", "success");
          this.getAllCabins();
        });
    } catch (e) {
      this.NotificationHandler("Unable to remove cabin", "danger");
    }
  };

  render() {
    const { cabins } = this.state;
    let FormData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="app-page">
        <span className="counts">Total Cabins: {this.state.cabinsCount}</span>
        <button
          type="button"
          className="btn btn-dark add-cabin"
          onClick={this.openNewModal.bind(this)}
        >
          <i className="fa fa-plus" title="Add Cabin"></i>
        </button>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Sr.No.</th>
              <th scope="col">Number</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {cabins.map((item, index) => (
              <tr className={item.cabin_no} key={index}>
                <td>{index + 1}</td>
                <td>{item.cabin_no}</td>
                <td>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.openModal.bind(this, item)}
                  >
                    <i className="fa fa-pencil" title="Edit"></i>
                  </button>
                </td>
                <td>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.removeCabin.bind(this, item._id)}
                  >
                    <i className="fa fa-trash" title="Remove"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        {FormData}
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Edit Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>Edit Cabin Number</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    defaultValue={this.state.cabin_no}
                    onChange={this.handleChange}
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    name="cabin_no"
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.updateCabinNo.bind(this)}
                  >
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>

        <Modal
          isOpen={this.state.newModalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="New Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>New Cabin Number</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    onChange={this.handleChange}
                    defaultValue="Meeting-room-"
                    placeholder="Enter cabin detail"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    name="cabin_no"
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.addCabinNo.bind(this)}
                  >
                    Save Cabin
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Cabin;
