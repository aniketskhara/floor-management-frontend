import React, { Component } from "react";
import axios from "axios";
import Modal from "react-modal";
import NotificationAlert from "react-notification-alert";
import { helpers } from "../../../../config";
import "./cabin.css";
import Loader from "react-loader-spinner";
import "../../home/vendor/overlayStyle.css";

const headers = {};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "35%"
  }
};

Modal.setAppElement("*");

class Role extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: [],
      modalIsOpen: false,
      newModalIsOpen: false,
      role: "",
      updated_role: "",
      role_id: "",
      rolesCount: 0,
      loading: false
    };
  }

  componentDidMount() {
    this.getAllRoles();
  }

  getAllRoles = () => {
    this.setState({ loading: true });
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/roles", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({
          roles: response.data.roles,
          rolesCount: response.data.rolesCount,
          loading: false
        });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch the roles", "danger");
      });
  };

  openModal = item => {
    this.setState({
      modalIsOpen: true
    });
    this.setItem(item);
  };

  openNewModal = () => {
    this.setState({
      newModalIsOpen: true
    });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, newModalIsOpen: false });
  };

  // On opening the modal set role id and no in props
  setItem(item) {
    this.setState({
      role_id: item._id,
      role: item.name
    });
  }

  handleChange = e => {
    this.setState({
      updated_role: e.target.value
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  // Update the role
  updateRole = async () => {
    let name = this.state.updated_role
    if ((/^[a-zA-Z]+$/.test(name))){
      try {
        await axios
          .patch(
            `https://shrouded-springs-70350.herokuapp.com/roles/${this.state.role_id}`,
            {
              name: this.state.updated_role
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.closeModal();
            this.NotificationHandler("Role updated successfully", "success");
            this.getAllRoles();
          });
      } catch (e) {
        this.NotificationHandler("Unable to update the role", "danger");
      }
    }
    else{
      this.NotificationHandler("Invalid Role Name", "danger");
    }
  };

  // Adding new role
  addRole = async () => {
    let name = this.state.updated_role
    if ((/^[a-zA-Z]+$/.test(name))){
      try {
        await axios
          .post(
            `https://shrouded-springs-70350.herokuapp.com/roles/`,
            {
              name: this.state.updated_role
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.closeModal();
            this.NotificationHandler("Role added successfully", "success");
            this.getAllRoles();
          });
      } catch (e) {
        this.NotificationHandler("Unable to add the role", "danger");
      }    
    }
    else{
      this.NotificationHandler("Invalid Role Name", "danger");
    }
  };

  // Remove role
  removeRole = async id => {
    try {
      await axios
        .delete(`https://shrouded-springs-70350.herokuapp.com/roles/${id}`, {
          headers: helpers.buildHeader()
        })
        .then(() => {
          this.closeModal();
          this.NotificationHandler("Role removed successfully", "success");
          this.getAllRoles();
        });
    } catch (e) {
      this.NotificationHandler("Unable to remove the role", "danger");
    }
  };

  render() {
    const { roles } = this.state;
    let FormData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="app-page">
        <span className="counts">Total Roles: {this.state.rolesCount}</span>
        <button
          type="button"
          className="btn btn-dark add-cabin"
          onClick={this.openNewModal.bind(this)}
        >
          <i className="fa fa-plus" title="Add Role"></i>
        </button>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Sr.No</th>
              <th scope="col">Role</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {roles.map((item, index) => (
              <tr className={item.role} key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.openModal.bind(this, item)}
                  >
                    <i className="fa fa-pencil" title="Edit"></i>
                  </button>
                </td>
                <td>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.removeRole.bind(this, item._id)}
                  >
                    <i className="fa fa-trash" title="Remove"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );

    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        {FormData}
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Edit Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>Edit Role</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    defaultValue={this.state.role}
                    onChange={this.handleChange}
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    class="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary update-cabin"
                    onClick={this.updateRole.bind(this)}
                  >
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
        <Modal
          isOpen={this.state.newModalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="New Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>New Role</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    onChange={this.handleChange}
                    placeholder="Enter the role"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    class="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.addRole.bind(this)}
                  >
                    Save Role
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Role;
