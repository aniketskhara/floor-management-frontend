import React, { Component } from "react";
import axios from "axios";
import Modal from "react-modal";
import NotificationAlert from "react-notification-alert";
import { helpers } from "../../../../config";
import "./cabin.css";
import Loader from "react-loader-spinner";
import "../../home/vendor/overlayStyle.css";

const headers = {};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "36%"
  }
};

Modal.setAppElement("*");

class Devise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      devises: [],
      modalIsOpen: false,
      newModalIsOpen: false,
      model_no: "",
      devise_id: "",
      devise_name: "",
      devise: "",
      all_os: [],
      os: "",
      devisesCount: 0,
      loading: false
    };
  }

  componentDidMount() {
    this.getAllDevises();
    this.getAllOs();
  }

  Options = ({ options }) => {
    return options.map(option => (
      <option key={option._id} value={option._id}>
        {option.name}
      </option>
    ));
  };

  getAllDevises = () => {
    this.setState({ loading: true });
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/devises", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({
          devises: response.data.devises,
          devisesCount: response.data.devisesCount,
          loading: false
        });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch the devices", "danger");
      });
  };

  getAllOs = () => {
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/os", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({ all_os: response.data.Os });
        this.state.all_os.push({ _id: "Select Os", name: "Select Os" });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch os", "danger");
      });
  };

  openModal = item => {
    this.setState({
      modalIsOpen: true
    });
    this.setItem(item);
  };

  openNewModal = () => {
    this.setState({
      newModalIsOpen: true
    });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, newModalIsOpen: false });
  };

  // On opening the modal set cabin id and no in props
  setItem(item) {
    this.setState({
      devise_id: item._id,
      devise: item,
      devise_name: item.name,
      model_no: item.model_no,
      os: item.os._id
    });
  }

  handleInputChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  // Update the devise
  updateDevise = async () => {
    let devise = this.state.devise_name;
    let model = this.state.model_no;
    let os =  this.state.os;
    
    if (this.validDevise(devise, model, os)) {
      try {
        await axios
          .patch(
            `https://shrouded-springs-70350.herokuapp.com/devises/${this.state.devise_id}`,
            {
              model_no: this.state.model_no,
              name: this.state.devise_name,
              os: this.state.os
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.clearStates();
            this.closeModal();
            this.NotificationHandler("Device updated successfully", "success");
            this.getAllDevises();
            this.clearStates();
          });
      } catch (e) {
        this.NotificationHandler("Unable to update devices", "danger");
      }
    }
  };

  clearStates() {
    this.setState({
      devise_name: "",
      model_no: "",
      os: "",
      devise: ""
    });
  }

  // Adding new cabin
  addDevise = async () => {
    let devise = this.state.devise_name;
    let model = this.state.model_no;
    let os =  this.state.os;

    if (this.validDevise(devise, model, os)) {
      try {
        await axios
          .post(
            `https://shrouded-springs-70350.herokuapp.com/devises/`,
            {
              name: this.state.devise_name,
              model_no: this.state.model_no,
              os: this.state.os
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.clearStates();
            this.closeModal();
            this.NotificationHandler("Device added successfully", "success");
            this.getAllDevises();
          });
      } catch (e) {
        this.NotificationHandler("Unable to add device", "danger");
      }
    }
  };

  validDevise = (name, imei, os) => {
    let checkForHexRegExp = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i
    if (!/^[a-zA-Z]+$/.test(name)){
      this.NotificationHandler("Please enter valid devise name", "danger");
    }
    else if (!/^\d{15}(,\d{15})*$/.test(imei)){
      this.NotificationHandler("Please enter valid IMEI number", "danger");
    }
    else if (!checkForHexRegExp.test(os)){
      this.NotificationHandler("Please select valid os", "danger");
    }
    else if (/^[a-zA-Z]+$/.test(name) && checkForHexRegExp.test(os) && /^\d{15}(,\d{15})*$/.test(imei)){
      return true;
    }
  };

  // Remove cabin
  removeCabin = async id => {
    try {
      await axios
        .delete(`https://shrouded-springs-70350.herokuapp.com/devises/${id}`, {
          headers: helpers.buildHeader()
        })
        .then(() => {
          this.closeModal();
          this.NotificationHandler("Device removed successfully", "success");
          this.getAllDevises();
        });
    } catch (e) {
      this.NotificationHandler("Unable to remove device", "danger");
    }
  };

  render() {
    const { devises } = this.state;
    let FormData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="app-page">
        <span className="counts">Total Devises: {this.state.devisesCount}</span>
        <button
          type="button"
          className="btn btn-dark add-cabin"
          onClick={this.openNewModal.bind(this)}
        >
          <i className="fa fa-plus" title="Add Devise"></i>
        </button>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Sr.No.</th>
              <th scope="col">IMEI</th>
              <th scope="col">Name</th>
              <th scope="col">Os</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {devises.map((item, index) => (
              <tr className={item.model_no} key={index}>
                <td>{index + 1}</td>
                <td>{item.model_no}</td>
                <td>{item.name}</td>
                <td>{item.os.name}</td>
                <td>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.openModal.bind(this, item)}
                  >
                    <i className="fa fa-pencil" title="Edit"></i>
                  </button>
                </td>
                <td>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.removeCabin.bind(this, item._id)}
                  >
                    <i className="fa fa-trash" title="Remove"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        {FormData}
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Edit Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>Edit Devise</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    name="model_no"
                    defaultValue={this.state.devise.model_no}
                    onChange={this.handleInputChange}
                    placeholder="IMEI"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <input
                    name="devise_name"
                    defaultValue={this.state.devise.name}
                    onChange={this.handleInputChange}
                    placeholder="devise name"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <select
                    className="form-control"
                    onChange={this.handleInputChange}
                    name="os"
                    defaultValue={this.state.os}
                  >
                    <this.Options options={this.state.all_os} />
                  </select>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-md-12 text-center">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.updateDevise.bind(this)}
                  >
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
        <Modal
          isOpen={this.state.newModalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="New Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>New Devise</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    name="model_no"
                    onChange={this.handleInputChange}
                    placeholder="IMEI"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <input
                    name="devise_name"
                    onChange={this.handleInputChange}
                    placeholder="devise name"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <select
                    className="form-control"
                    onChange={this.handleInputChange}
                    name="os"
                    defaultValue="Select Os"
                  >
                    <this.Options options={this.state.all_os} />
                  </select>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-md-12 text-center">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.addDevise.bind(this)}
                  >
                    Save Device
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Devise;
