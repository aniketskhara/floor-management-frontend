import React, { Component } from "react";
import axios from "axios";
import Modal from "react-modal";
import NotificationAlert from "react-notification-alert";
import { helpers } from "../../../../config";
import "./cabin.css";
import Loader from "react-loader-spinner";
import "../../home/vendor/overlayStyle.css";

const headers = {};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "35%"
  }
};

Modal.setAppElement("*");

class Technology extends Component {
  constructor(props) {
    super(props);
    this.state = {
      technologies: [],
      modalIsOpen: false,
      newModalIsOpen: false,
      technology: "",
      updated_technology: "",
      technology_id: "",
      technologiesCount: 0,
      loading: false
    };
  }

  componentDidMount() {
    this.getAlltechnologies();
  }

  getAlltechnologies = () => {
    this.setState({ loading: true });
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/technologies", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({
          technologies: response.data.technologies,
          technologiesCount: response.data.technologiesCount,
          loading: false
        });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch the technologies", "danger");
      });
  };

  openModal = item => {
    this.setState({
      modalIsOpen: true
    });
    this.setItem(item);
  };

  openNewModal = () => {
    this.setState({
      newModalIsOpen: true
    });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, newModalIsOpen: false });
  };

  // On opening the modal set technology id and no in props
  setItem(item) {
    this.setState({
      technology_id: item._id,
      technology: item.name
    });
  }

  handleChange = e => {
    this.setState({
      updated_technology: e.target.value
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  // Update the technology
  updateTechnology = async () => {
    let name = this.state.updated_technology
    
    if (!/^[a-zA-Z]+$/.test(name)){
      this.NotificationHandler("Please enter valid technology name", "danger");
    }
    else {
      try {
        await axios
          .patch(
            `https://shrouded-springs-70350.herokuapp.com/technologies/${this.state.technology_id}`,
            {
              name: this.state.updated_technology
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.closeModal();
            this.NotificationHandler(
              "Technology updated successfully",
              "success"
            );
            this.getAlltechnologies();
            this.setState({
              updated_technology: ""
            });
          });
      } catch (e) {
        this.NotificationHandler("Unable to update the technology", "danger");
      }
    }
  };

  // Adding new technology
  addTechnology = async () => {
    let name = this.state.updated_technology
    
    if (!/^[a-zA-Z]+$/.test(name)){
      this.NotificationHandler("Please enter valid technology name", "danger");
    }
    else {
      try {
        await axios
          .post(
            `https://shrouded-springs-70350.herokuapp.com/technologies/`,
            {
              name: this.state.updated_technology
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.closeModal();
            this.NotificationHandler("Technology added successfully", "success");
            this.getAlltechnologies();
            this.setState({
              updated_technology: ""
            });
          });
      } catch (e) {
        this.NotificationHandler("Unable to add the technology", "danger");
      }
    }
  };

  // Remove technology
  removeTechnology = async id => {
    try {
      await axios
        .delete(
          `https://shrouded-springs-70350.herokuapp.com/technologies/${id}`,
          {
            headers: helpers.buildHeader()
          }
        )
        .then(() => {
          this.closeModal();
          this.NotificationHandler(
            "Technology removed successfully",
            "success"
          );
          this.getAlltechnologies();
        });
    } catch (e) {
      this.NotificationHandler("Unable to remove the technology", "danger");
    }
  };

  render() {
    const { technologies } = this.state;
    let FormData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="app-page">
        <span className="counts">
          Total Technologies: {this.state.technologiesCount}
        </span>

        <button
          type="button"
          className="btn btn-dark add-cabin"
          onClick={this.openNewModal.bind(this)}
        >
          <i className="fa fa-plus" title="Add Technology"></i>
        </button>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Sr.No</th>
              <th scope="col">Technology</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {technologies.map((item, index) => (
              <tr className={item.technology} key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.openModal.bind(this, item)}
                  >
                    <i className="fa fa-pencil" title="Edit"></i>
                  </button>
                </td>
                <td>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.removeTechnology.bind(this, item._id)}
                  >
                    <i className="fa fa-trash" title="Remove"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        {FormData}
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Edit Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>Edit Technology</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    defaultValue={this.state.technology}
                    onChange={this.handleChange}
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.updateTechnology.bind(this)}
                  >
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
        <Modal
          isOpen={this.state.newModalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="New Modal"
          shouldCloseOnOverlayClick={true}
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>New Technology</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    onChange={this.handleChange}
                    placeholder="Enter tecchnology"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.addTechnology.bind(this)}
                  >
                    Save Technology
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Technology;
