import React, { Component } from "react";
import { Switch, Link, Route, withRouter } from "react-router-dom";
import PropTypes from "prop-types";

import { appContants } from "../../../../config";
import Cabin from "./cabin";
import Role from "./role";
import Devise from "./devise";
import Technology from "./technology";
import Department from "./department";
import "./style.css";

const { master_routes } = appContants;

class Master extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heading: 'Cabins',
      activeRoute: master_routes[0].title
    };
  }

  componentDidMount() {
    const { history } = this.props;

    history.listen(location => {
      const activeRoute = master_routes.find(
        item => item.route === location.pathname
      );
      if (activeRoute) {
        this.setState({
          heading: activeRoute.title,
          activeRoute: activeRoute.title,
        });
        // Update the subtitle 
        this.props.triggerUpdateMaster(activeRoute.title);
      }
    });
  }

  render() {
    const { activeRoute } = this.state;
    const { match } = this.props;
    const { path } = match;

    return (
      <div className="app-page">
        <ul className="master-list">
          {master_routes.map((item, index) => (
            <li
              className={activeRoute === item.title ? `active master-${index}` : ""}
              key={index}
            >
              <Link to={`${item.route}`}>{item.title}</Link>
            </li>
          ))}
        </ul>
        <Switch>
          <Route exact path={`${path}/cabins`}>
            <Cabin />
          </Route>
          <Route exact path={`${path}/roles`}>
            <Role />
          </Route>
          <Route exact path={`${path}/devises`}>
            <Devise />
          </Route>
          <Route exact path={`${path}/technologies`}>
            <Technology />
          </Route>
          <Route exact path={`${path}/departments`}>
            <Department />
          </Route>
          <Route path="*" component={Cabin} />
        </Switch>
      </div>
    );
  }
}

// export default Master;
Master.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired
};
export default withRouter(Master);
