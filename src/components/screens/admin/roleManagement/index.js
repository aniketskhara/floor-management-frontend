import React, { Component } from "react";
import axios from "axios";
import NotificationAlert from "react-notification-alert";
import "../master/style.css";
import { appContants, helpers } from "../../../../config";
// import Checkbox from './Checkbox';

const { routes } = appContants;

class RoleManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: [],
      access: [],
      selected_role: "",
      selecte_role_name: "",
      isChecked: false,
      action: "hide"
    };
  }

  componentDidMount() {
    this.getAllRoles();
  }

  Options = ({ options }) => {
    return options.map(option => (
      <option key={option._id} value={option._id} data-id={option.name}>
        {option.name}
      </option>
    ));
  };

  getAllRoles = () => {
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/roles", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({
          roles: [
            { _id: "Select Role", name: "Select Role" },
            ...response.data.roles
          ]
        });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch roles", "danger");
      });
  };

  handleChange = async event => {
    const target = event.target;
    const value = target.value;

    const index = event.target.selectedIndex;
    const optionElement = event.target.childNodes[index];
    const option = optionElement.getAttribute("data-id");

    await this.setSelectedValue(value, option);

    if (value == "Select Role") {
      this.setState({
        action: "hide"
      });
    } else {
      this.getScreens(value);
      this.setState({
        action: "show"
      });
    }
  };

  setSelectedValue = (value, option) => {
    this.setState({
      selected_role: value,
      selected_role_name: option
    });
  };

  getScreens = id => {
    axios
      .get(
        `https://shrouded-springs-70350.herokuapp.com/role-management/role-item/${id}`,
        {
          headers: helpers.buildHeader()
        }
      )
      .then(response => {
        this.setState({ access: response.data.RolesWithAccess.access });
      })
      .catch(error => {
        this.NotificationHandler(
          "Permissions not present for selected role",
          "danger"
        );
        this.setState({
          access: []
        });
      });
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  toggleChange = e => {
    const { access } = this.state;
    if (this.state.selected_role_name != "admin") {
      this.setState({
        isChecked: access.includes(e.target.value) ? false : true,
        access: access.includes(e.target.value)
          ? access.filter(item => item !== e.target.value)
          : [...access, e.target.value]
      });
    }
  };

  permitted = value => {
    let permissions = this.state.access;
    return permissions.includes(value);
  };

  // Update permissions
  updateAccess = async () => {
    try {
      await axios
        .patch(
          `https://shrouded-springs-70350.herokuapp.com/role-management/role-item/${this.state.selected_role}`,
          {
            role_id: this.state.selected_role,
            access: this.state.access
          },
          { headers: helpers.buildHeader() }
        )
        .then(() => {
          this.NotificationHandler(
            "Permissions Updated Successfully",
            "success"
          );
        });
    } catch (e) {
      this.NotificationHandler("Permissions not updated", "danger");
    }
  };

  render() {
    const { roles } = this.state;
    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        <div className="app-page">
          <div className="role-management">
            <div className="row">
              <div className="col-md-3">
                <select
                  className="form-control"
                  name="selected_role"
                  defaultValue="Select Role"
                  onChange={this.handleChange}
                >
                  <this.Options options={roles} />
                </select>
              </div>
              <div className="col">
                <button
                  type="button"
                  className="btn btn-secondary set-permissions"
                  onClick={this.updateAccess.bind(this)}
                >
                  Update
                </button>
              </div>
            </div>
            <ul className={`access-list ${this.state.action}`}>
              {routes.map((item, index) => (
                <li className="permission-list" key={index}>
                  <label>
                    <input
                      type="checkbox"
                      className="permissions"
                      checked={this.permitted(item.value)}
                      onChange={this.toggleChange}
                      value={item.value}
                    />
                    {item.title}
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default RoleManagement;
