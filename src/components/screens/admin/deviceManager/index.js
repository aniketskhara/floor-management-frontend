import React, { Component } from "react";
import Scheduler, { SchedulerData, ViewTypes } from "react-big-scheduler";
import "react-big-scheduler/lib/css/style.css";
import Modal from "react-modal";
import "./calendar.css";
import moment from "moment";
import withDragDropContext from "./withDnDContext";
import axios from "axios";
import DateTimePicker from "react-datetime-picker";
import NotificationAlert from "react-notification-alert";
import { makeStyles } from "@material-ui/core/styles";
import { helpers } from "../../../../config";
import Loader from "react-loader-spinner";
import "../../home/vendor/overlayStyle.css";

const customStyles = {
  content: {
    top: "10%",
    // left: "54%",
    // right: "auto",
    // bottom: "auto",
    // marginRight: "-50%",
    // transform: "translate(-50%, -50%)",
    margin: "auto",
    width: "40%",
    height: "300px"
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 200
    }
  }
}));

const headers = {};

let events = [];
class DeviceManager extends Component {
  constructor(props) {
    super(props);

    let schedulerData = new SchedulerData(
      new Date(),
      ViewTypes.Week,
      false,
      false,
      {
        checkConflict: true
      }
    );

    this.state = {
      viewModel: schedulerData,
      devises: [],
      all_os: [],
      selectDevises: [],
      modalIsOpen: false,
      title: "",
      start: new Date(),
      end: new Date(),
      resourceId: "",
      user_id: "",
      events: [],
      eventModalIsOpen: false,
      currentEvent: "",
      employee: "",
      loading: false
    };

    schedulerData.localeMoment.locale("en");
    schedulerData.config.schedulerWidth = "80%";
    schedulerData.config.resourceName = "Devise (os, model no)";
  }

  componentDidMount() {
    this.getAllDevises();
    this.getAllEvents();
  }

  Options = ({ options }) => {
    return options.map(option => (
      <option key={option._id} value={option._id}>
        {option.model_no + " ( " + option.os.name + " ) "}
      </option>
    ));
  };

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  submitHandler = e => {
    e.preventDefault();
  };

  getAllDevises = async () => {
    try {
      await axios
        .get("https://shrouded-springs-70350.herokuapp.com/devises", {
          headers: helpers.buildHeader()
        })
        .then(response => {
          this.setState({
            devises: response.data.devises,
            selectDevises: [
              { _id: "", model_no: "Select Devise", os: { name: "os" } },
              ...response.data.devises
            ]
          });
          this.setResources(this.state.devises);
        });
    } catch (err) {
      this.NotificationHandler("Unable to fetch devises", "danger");
    }
  };

  setResources = devises => {
    let schedulerData = this.state.viewModel;
    let newDevises = [];
    let len = devises.length;
    for (var i = 0; i < len; i++) {
      newDevises.push({
        id: devises[i]._id,
        name:
          devises[i].name +
          " ( " +
          devises[i].os.name +
          ", model no: " +
          devises[i].model_no +
          " )"
      });
    }
    schedulerData.setResources(newDevises);
    this.setState({ viewModel: schedulerData });
  };

  openModal = () => {
    this.setState({ modalIsOpen: true });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };

  openViewModal = event => {
    this.setState({
      currentEvent: event,
      eventModalIsOpen: true
    });
  };

  closeViewModal = () => {
    this.setState({ eventModalIsOpen: false });
  };

  prevClick = schedulerData => {
    schedulerData.prev();
    this.setEvents(this.state.events);
    this.setState({
      viewModel: schedulerData
    });
  };

  nextClick = schedulerData => {
    schedulerData.next();
    this.setEvents(this.state.events);
    this.setState({
      viewModel: schedulerData
    });
  };

  onViewChange = (schedulerData, view) => {
    schedulerData.setViewType(
      view.viewType,
      view.showAgenda,
      view.isEventPerspective
    );
    this.setEvents(this.state.events);
    this.setState({
      viewModel: schedulerData
    });
  };

  onSelectDate = (schedulerData, date) => {
    schedulerData.setDate(date);
    this.setEvents(this.state.events);
    this.setState({
      viewModel: schedulerData
    });
  };

  eventClicked = (schedulerData, event) => {
    this.setState({
      employee: event.employee
    });
    this.openViewModal(event);
  };

  onScrollRight = (schedulerData, schedulerContent, maxScrollLeft) => {
    if (schedulerData.ViewTypes === ViewTypes.Day) {
      schedulerData.next();
      this.setEvents(this.state.events);
      this.setState({
        viewModel: schedulerData
      });

      schedulerContent.scrollLeft = maxScrollLeft - 10;
    }
  };

  onScrollLeft = (schedulerData, schedulerContent, maxScrollLeft) => {
    if (schedulerData.ViewTypes === ViewTypes.Day) {
      schedulerData.prev();
      this.setEvents(this.state.events);
      this.setState({
        viewModel: schedulerData
      });

      schedulerContent.scrollLeft = 10;
    }
  };

  toggleExpandFunc = (schedulerData, slotId) => {
    schedulerData.toggleExpandStatus(slotId);
    this.setState({
      viewModel: schedulerData
    });
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  onStartChange = date => {
    this.setState({ start: date });
  };

  onEndChange = date => {
    this.setState({ end: date });
  };

  deviseAvailable = () => {
    let start = this.state.start;
    let end = this.state.end;

    if ((start > new Date()) && (start < end)){
      let allEvents = this.state.events
      let selectedDateEvents = allEvents.filter(item => ((new Date(item.start).toDateString() == this.state.start.toDateString()) && (item.deviseId === this.state.resourceId)));
      this.checkTimeSlots(selectedDateEvents, this.state.start, this.state.end)
    } 
    else {
      this.NotificationHandler("Start time cannot be greater End Time", "danger");
    }
  };

  checkTimeSlots = (similarEvents, startTime, endTime) => {
    let duration;
    let durationsWithEvents = [];
    for (let i = 0; i < similarEvents.length; i++) {
      duration =
        new Date(similarEvents[i].end) - new Date(similarEvents[i].start);
      durationsWithEvents.push({
        start: similarEvents[i].start,
        end: similarEvents[i].end,
        duration: this.millisToMinutesAndSeconds(duration),
        deviseId: similarEvents[i].deviseId
      });
    }
    this.createDummmyTime(startTime, endTime, durationsWithEvents);
  };

  millisToMinutesAndSeconds = millis => {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return parseInt(minutes + ":" + (seconds < 10 ? "0" : "") + seconds);
  };

  createDummmyTime = (startTime, endTime, dEvents) => {
    let dummyTime = [];

    for (let i = 0; i < dEvents.length; i++) {
      for (let g = 0; g < dEvents[i].duration; g++) {
        var now = new Date(dEvents[i].start);
        now.setMinutes(now.getMinutes() + g);
        now = new Date(now);
        dummyTime.push(now.getHours() + ":" + now.getMinutes());
      }
    }

    let selectedStartTime = startTime.getHours() + ":" + startTime.getMinutes();
    let selectedEndTime = endTime.getHours() + ":" + endTime.getMinutes();

    if (dummyTime.includes(selectedStartTime)){

      this.NotificationHandler("Devise already occupied, Please insert valid start time", "danger");
    }
    else if (dummyTime.includes(selectedEndTime)){

      this.NotificationHandler("Devise already occupied, Please insert valid end time", "danger");
    }
    else if ((dummyTime[dummyTime.length - 1] < selectedEndTime) && (selectedStartTime < dummyTime[0])){

      this.NotificationHandler("Devise already occupied, Please insert valid time", "danger");
    }
    else {
      this.bookDevise();
    }
  };

  bookDevise = async () => {
    let current_datetime = new Date();
    const existing_bookings = this.state.events.find(
      existing_booking =>
        new Date(existing_booking.start).getTime() ===
          this.state.start.getTime() &&
        existing_booking.deviseId === this.state.resourceId
    );

    const existingBetween = this.state.events.find(
      existingBook =>
        moment(this.state.start).isBetween(
          existingBook.start,
          existingBook.end
        ) ||
        (moment(this.state.end).isBetween(
          existingBook.start,
          existingBook.end
        ) &&
          existingBook.deviseId === this.state.resourceId)
    );

    const existingBetweenDevice = this.state.events.find(
      existingBook => existingBook.deviseId === this.state.resourceId
    );
    if (
      existing_bookings ||
      this.state.start < current_datetime ||
      this.state.end < current_datetime ||
      (existingBetween && existingBetweenDevice)
    ) {
      this.NotificationHandler(
        "Please select the valid date and time or check if devise is already booked",
        "danger"
      );
    } else {
      try {
        await axios
          .post(
            `https://shrouded-springs-70350.herokuapp.com/devise-management/`,
            {
              title: this.state.title,
              deviseId: this.state.resourceId,
              start: this.state.start,
              end: this.state.end,
              employee_id: localStorage.getItem("user_id")
            },
            { headers: helpers.buildHeader() }
          )
          .then(res => {
            this.closeModal();
            this.setState({
              title: "",
              deviseId: ""
            });
            this.NotificationHandler("Devise booked successfully", "success");
            this.getAllEvents();
          });
      } catch (err) {
        this.mapErrors(err.response.data.err);
      }
    }
  };

  mapErrors = str => {
    var res = str.replace("DeviseManagement validation failed:", "");
    var array = res.split(",");
    for (let i = 0; i < array.length; i++) {
      let keyMsg;
      let msg;
      keyMsg = array[i];
      msg = keyMsg.replace(/^.+:/, "");
      this.NotificationHandler(msg, "danger");
    }
  };

  getAllEvents = async () => {
    this.setState({ loading: true });

    try {
      await axios
        .get(
          `https://shrouded-springs-70350.herokuapp.com/devise-management/`,
          { headers: helpers.buildHeader() }
        )
        .then(res => {
          this.setState({
            events: res.data.DeviseEvents,
            loading: false
          });
          this.setEvents(this.state.events);
        });
    } catch (err) {
      this.NotificationHandler("Unable to fetch events", "danger");
    }
  };

  setEvents = events => {
    let schedulerData = this.state.viewModel;
    let newEvents = [];
    let len = events.length;
    for (var i = 0; i < len; i++) {
      newEvents.push({
        id: events[i]._id,
        resourceId: events[i].deviseId,
        start: events[i].start,
        end: events[i].end,
        title: events[i].title,
        employee: events[i].employee_id
      });
    }
    schedulerData.setEvents(newEvents);
    this.setState({ viewModel: schedulerData });
  };

  getTime = data => {
    var date = new Date(data);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;
    return strTime;
  };

  getDate = data => {
    var date = new Date(data);
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var strDate = day + " / " + month + " / " + year;
    return strDate;
  };

  render() {
    const { viewModel, employee } = this.state;

    let CalendarData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="app-page">
        <button
          type="button"
          className="btn btn-dark add-cabin"
          onClick={this.openModal}
        >
          <i className="fa fa-plus" title="Add Cabin"></i>
        </button>
      </div>
    );

    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        <div>
          {CalendarData}
          <div className="rbc-calendar">
            <Scheduler
              schedulerData={viewModel}
              prevClick={this.prevClick}
              nextClick={this.nextClick}
              onSelectDate={this.onSelectDate}
              onViewChange={this.onViewChange}
              eventItemClick={this.eventClicked}
              viewEventClick={this.ops1}
              viewEventText="Ops 1"
              viewEvent2Text="Ops 2"
              viewEvent2Click={this.ops2}
              updateEventStart={this.updateEventStart}
              updateEventEnd={this.updateEventEnd}
              moveEvent={this.moveEvent}
              newEvent={this.newEvent}
              onScrollLeft={this.onScrollLeft}
              onScrollRight={this.onScrollRight}
              onScrollTop={this.onScrollTop}
              onScrollBottom={this.onScrollBottom}
              toggleExpandFunc={this.toggleExpandFunc}
            />
          </div>

          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Add Event"
            shouldCloseOnOverlayClick={true}
          >
            <button
              onClick={this.closeModal}
              type="button"
              className="close"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <h3>Book Device</h3>
            <hr />
            <form action="#" onSubmit={this.submitHandler}>
              <div className="row">
                <div className="col-md-6">
                  <input
                    onChange={this.handleChange}
                    placeholder="Enter title"
                    name="title"
                    className="form-control"
                  />
                </div>
                <div className="col-md-6">
                  <select
                    className="form-control"
                    class="form-control"
                    onChange={this.handleChange}
                    name="resourceId"
                  >
                    <this.Options options={this.state.selectDevises} />
                  </select>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <DateTimePicker
                    onChange={this.onStartChange}
                    value={this.state.start}
                  />
                </div>
                <div className="col-md-6">
                  <DateTimePicker
                    onChange={this.onEndChange}
                    value={this.state.end}
                  />
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-md-12 text-center">
                  <button
                    className="btn btn-dark"
                    onClick={this.deviseAvailable}
                  >
                    Book
                  </button>
                </div>
              </div>
            </form>
          </Modal>

          <Modal
            isOpen={this.state.eventModalIsOpen}
            onAfterOpen={this.eventAfterOpenModal}
            onRequestClose={this.closeViewModal}
            style={customStyles}
            contentLabel="View Event"
            shouldCloseOnOverlayClick={true}
          >
            <button
              onClick={this.closeViewModal}
              type="button"
              className="close"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <h3>Booked Device Details</h3>
            <div>
              <div className="row">
                <div className="col-sm-6">
                  <label>Title: {this.state.currentEvent.title}</label>
                </div>
                <div className="col-sm-6">
                  <label>
                    Booked by: {employee.firstName + " " + employee.lastName}
                  </label>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <label>
                    Start time:&nbsp;
                    {this.getTime(this.state.currentEvent.start)}
                  </label>
                </div>
                <div className="col-sm-6">
                  <label>mob: {employee.contactNumber}</label>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <label>
                    End time:&nbsp;
                    {this.getTime(this.state.currentEvent.end)}
                  </label>
                </div>
                <div className="col-sm-6">
                  <label>Skype Id: {employee.skype_id}</label>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <label>
                    Start Date:&nbsp;
                    {this.getDate(this.state.currentEvent.start)}
                  </label>
                </div>
                <div className="col-sm-6">
                  <label>
                    End Date:&nbsp;
                    {this.getDate(this.state.currentEvent.end)}
                  </label>
                </div>
              </div>
            </div>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

export default withDragDropContext(DeviceManager);
