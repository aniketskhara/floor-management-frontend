import React, { Component } from "react";
import axios from "axios";
import Modal from "react-modal";
import NotificationAlert from "react-notification-alert";
import { helpers } from "../../../../config";
import "../master/style.css";
import Loader from "react-loader-spinner";
import "../../home/vendor/overlayStyle.css";
import { ValidationsError } from "../../../custom";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "36%"
  }
};

const headers = {};

Modal.setAppElement("*");
class Employee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employees: [],
      roles: [],
      modalIsOpen: false,
      newModalIsOpen: false,
      employeeId: "",
      firstName: "",
      lastName: "",
      email: "",
      department: "",
      skypeId: "",
      contactNumber: "",
      experience: "",
      password: "",
      employee: "",
      role: "",
      departments: [],
      filterDepartment: "",
      employeeCount: 0,
      selectedDepartment: "",
      loading: false,
      errors: {},
      isFormValid: true
    };
  }

  componentDidMount() {
    this.getAllEmployees();
    this.getAllDeaprtments();
    this.getAllRoles();
  }

  Options = ({ options }) => {
    return options.map(option => (
      <option key={option._id} value={option._id}>
        {option.name}
      </option>
    ));
  };

  getAllEmployees = () => {
    this.setState({ loading: true });
    if (this.state.filterDepartment !== "Select All") {
      axios
        .get(
          `https://shrouded-springs-70350.herokuapp.com/employees?department=${this.state.filterDepartment}`,
          {
            headers: helpers.buildHeader()
          }
        )
        .then(response => {
          this.setState({
            employees: response.data.employees,
            employeeCount: response.data.EmployeeCount,
            loading: false
          });
        })
        .catch(error => {
          this.NotificationHandler("Unable to fetch employees", "danger");
        });
    } else {
      axios
        .get(`https://shrouded-springs-70350.herokuapp.com/employees`, {
          headers: helpers.buildHeader()
        })
        .then(response => {
          this.setState({
            employees: response.data.employees,
            employeeCount: response.data.EmployeeCount
          });
        })
        .catch(error => {
          this.NotificationHandler("Unable to fetch employees", "danger");
        });
    }
  };

  getAllDeaprtments = () => {
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/departments", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({ departments: response.data.departments });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch departments", "danger");
      });
  };

  getAllRoles = () => {
    axios
      .get("https://shrouded-springs-70350.herokuapp.com/roles", {
        headers: helpers.buildHeader()
      })
      .then(response => {
        this.setState({ roles: response.data.roles });
      })
      .catch(error => {
        this.NotificationHandler("Unable to fetch roles", "danger");
      });
  };

  openModal = item => {
    this.setState({
      modalIsOpen: true
    });
    this.setItem(item);
  };

  openNewModal = () => {
    this.setState({
      newModalIsOpen: true
    });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, newModalIsOpen: false });
  };

  // On opening the modal set cabin id and no in props
  setItem(item) {
    this.setState({
      employee: item,
      employeeId: item._id,
      firstName: item.firstName,
      lastName: item.lastName,
      email: item.email,
      skypeId: item.skype_id,
      contactNumber: item.contactNumber,
      experience: item.experience,
      role: item.role,
      department: item.department._id
    });
  }

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    console.log(name + "  " + value);
    this.setState({
      [name]: value
    });
  };

  handleFilterChange = async event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    await this.setState({ filterDepartment: value });
    this.getAllEmployees();
    // this.setState({ employeeCount: 0 });
  };

  clearStates = () => {
    this.setState({
      employee: "",
      employeeId: "",
      firstName: "",
      lastName: "",
      email: "",
      skypeId: "",
      contactNumber: "",
      experience: "",
      role: "",
      department: "",
      isFormValid: true
    });
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  CustomNotificationHandler = (messages, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: <ValidationsError messages={messages} />,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };
  // Update the cabin no
  updateEmployee = async () => {
    let fName = this.state.firstName;
    let lName = this.state.lastName;
    let emid = this.state.email;
    let s_id = this.state.skypeId;
    let cNumber = this.state.contactNumber;
    let exp = this.state.experience;
    let pass = this.state.password;
    let role_id = this.state.role;
    let department_id = this.state.department;
    if (
      this.validateForm(
        fName,
        lName,
        emid,
        s_id,
        cNumber,
        exp,
        pass,
        role_id,
        department_id
      )
    ) {
      try {
        await axios
          .patch(
            `https://shrouded-springs-70350.herokuapp.com/employees/${this.state.employeeId}`,
            {
              firstName: this.state.firstName,
              lastName: this.state.lastName,
              email: this.state.email,
              department: this.state.department,
              skype_id: this.state.skypeId,
              contactNumber: this.state.contactNumber,
              experience: this.state.experience,
              role: this.state.role
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.closeModal();
            this.NotificationHandler(
              "Employee updated successfully",
              "success"
            );
            this.clearStates();
            this.getAllEmployees();
          });
      } catch (e) {
        var str = e.response.data.err;
        this.mapErrors(str);
      }
    }
  };

  // Adding new cabin
  addEmployee = async () => {
    let fName = this.state.firstName;
    let lName = this.state.lastName;
    let emid = this.state.email;
    let s_id = this.state.skypeId;
    let cNumber = this.state.contactNumber;
    let exp = this.state.experience;
    let pass = this.state.password;
    let role_id = this.state.role;
    let department_id = this.state.department;

    if (
      this.validateForm(
        fName,
        lName,
        emid,
        s_id,
        cNumber,
        exp,
        pass,
        role_id,
        department_id
      )
    ) {
      try {
        await axios
          .post(
            `https://shrouded-springs-70350.herokuapp.com/employees/`,
            {
              firstName: this.state.firstName,
              lastName: this.state.lastName,
              email: this.state.email,
              skype_id: this.state.skypeId,
              contactNumber: this.state.contactNumber,
              experience: this.state.experience,
              password: this.state.password,
              role: this.state.role,
              department: this.state.department
            },
            { headers: helpers.buildHeader() }
          )
          .then(() => {
            this.setState({ errors: {} });
            this.closeModal();
            this.NotificationHandler("Employee added successfully", "success");
            this.clearStates();
            this.getAllEmployees();
          });
      } catch (e) {
        var str = e.response.data.err;
        this.mapErrors(str);
      }
    }
  };

  // Get all errors and map them for displaying properly
  mapErrors = str => {
    var res = str.replace(
      'Employee validation failed: role: Cast to ObjectID failed for value "" at path "role", department: Cast to ObjectID failed for value "" at path "department",',
      ""
    );
    var array = res.split(",");
    for (let i = 0; i < array.length; i++) {
      let keyMsg;
      let msg;
      keyMsg = array[i];
      msg = keyMsg.replace(/^.+:/, "");
      this.NotificationHandler(msg, "danger");
    }
  };

  validateForm = (
    fname,
    lname,
    emid,
    s_id,
    cNumber,
    exp,
    pass,
    role_id,
    department_id
  ) => {
    let validationArr = [];

    var regExp = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
    if (!/^[a-zA-Z]+$/.test(fname)) {
      validationArr.push("Please enter valid first name");
    }
    if (!/^[a-zA-Z]+$/.test(lname)) {
      validationArr.push("Please enter valid last name");
    }
    if (!regExp.test(emid)) {
      validationArr.push("Please enter valid email id");
    }
    if (!s_id.length > 0) {
      validationArr.push("Please enter valid skype id");
    }
    if (!/^[0-9]{10}$/.test(cNumber)) {
      validationArr.push("Please enter valid contact number");
    }
    if (!exp.length > 0) {
      validationArr.push("Please enter valid experience");
    }
    if (!pass.length >= 6) {
      validationArr.push("Please enter valid password");
    }
    if (!role_id.length > 0) {
      validationArr.push("Please select valid role");
    }
    if (!department_id.length > 0) {
      validationArr.push("Please select valid department");
    }
    if (validationArr.length > 0) {
      this.CustomNotificationHandler(validationArr, "danger");
    }
    return validationArr.length > 0 ? false : true;
  };

  submitHandler = e => {
    e.preventDefault();
  };

  // Remove cabin
  removeCabin = async id => {
    try {
      await axios
        .delete(
          `https://shrouded-springs-70350.herokuapp.com/employees/${id}`,
          {
            headers: helpers.buildHeader()
          }
        )
        .then(() => {
          this.closeModal();
          this.NotificationHandler("Employee removed successfully", "success");
          this.getAllEmployees();
        });
    } catch (e) {
      this.NotificationHandler("Unable to remove employee", "danger");
    }
  };

  render() {
    const { employees } = this.state;
    let FormData = this.state.loading ? (
      <div className="overlay-backdrop">
        <div className="overlay">
          <div className="loader-position">
            <Loader type="Oval" color="#d41872" height={100} width={100} />
          </div>
        </div>
      </div>
    ) : (
      <div className="app-page">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Sr.No</th>
              <th scope="col">Email</th>
              <th scope="col">Name</th>
              <th scope="col">Department</th>
            </tr>
          </thead>
          <tbody>
            {employees.map((item, index) => (
              <tr className={item._id} key={index}>
                <td>{index + 1}</td>
                <td>{item.email}</td>
                <td>{item.firstName + "  " + item.lastName}</td>
                <td>{item.department.name}</td>
                <td>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.openModal.bind(this, item)}
                  >
                    <i className="fa fa-pencil" title="Edit"></i>
                  </button>
                </td>
                <td>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.removeCabin.bind(this, item._id)}
                  >
                    <i className="fa fa-trash" title="Remove"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
    return (
      <React.Fragment>
        <NotificationAlert ref="notify" />
        <span className="counts">
          Total Employee: {this.state.employeeCount}
        </span>

        <span>
          <select
            className="role-management-select-box"
            name="filterDepartment"
            onChange={this.handleFilterChange}
          >
            <option selected disabled={true}>
              All Departments
            </option>
            <this.Options options={this.state.departments} />
          </select>
        </span>
        <button
          type="button"
          className="btn btn-dark add-cabin"
          onClick={this.openNewModal.bind(this)}
        >
          <i className="fa fa-user-plus" title="Add Employee"></i>
        </button>
        {FormData}
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Edit Modal"
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>Edit Employee</h3>
          <hr />
          <div className="employee-edit">
            <form action="#" onSubmit={this.submitHandler} method="post">
              <div className="row">
                <div className="col-md-6">
                  <input
                    defaultValue={this.state.employee.firstName}
                    onChange={this.handleChange}
                    placeholder="first name"
                    name="firstName"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                  <div className="errorMsg">{this.state.errors.firstName}</div>
                </div>
                <div className="col-md-6">
                  <input
                    defaultValue={this.state.employee.lastName}
                    onChange={this.handleChange}
                    placeholder="last name"
                    name="lastName"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                  <div className="errorMsg">{this.state.errors.lastName}</div>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <input
                    defaultValue={this.state.employee.email}
                    onChange={this.handleChange}
                    placeholder="email"
                    name="email"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                    className="form-control"
                  />
                  <div className="errorMsg">{this.state.errors.email}</div>
                </div>
                <div className="col-md-6">
                  <select
                    className="form-control"
                    onChange={this.handleChange}
                    name="department"
                    defaultValue={this.state.department}
                  >
                    <option disabled={true}>Select Department</option>
                    <this.Options options={this.state.departments} />
                  </select>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    defaultValue={this.state.employee.skype_id}
                    onChange={this.handleChange}
                    placeholder="skype id"
                    name="skypeId"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.skypeId}</div>
                </div>
                <div className="col-md-6">
                  <input
                    className="form-control"
                    defaultValue={this.state.employee.contactNumber}
                    onChange={this.handleChange}
                    placeholder="contact no"
                    name="contactNumber"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">
                    {this.state.errors.contactNumber}
                  </div>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    defaultValue={this.state.employee.experience}
                    onChange={this.handleChange}
                    placeholder="experience"
                    name="experience"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.experience}</div>
                </div>
                <div className="col-md-6">
                  <select
                    className="form-control"
                    onChange={this.handleChange}
                    name="role"
                    defaultValue={this.state.employee.role}
                  >
                    <option disabled={true}>Select Role</option>
                    <this.Options options={this.state.roles} />
                  </select>
                  <div className="errorMsg">{this.state.errors.role}</div>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-md-12 text-center">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.updateEmployee.bind(this)}
                  >
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
        <Modal
          isOpen={this.state.newModalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="New Modal"
        >
          <button
            onClick={this.closeModal}
            type="button"
            className="close"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>New Employee</h3>
          <hr />
          <div className="cabin-edit">
            <form action="#" onSubmit={this.submitHandler} method="post">
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="first name"
                    name="firstName"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.firstName}</div>
                </div>
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="last name"
                    name="lastName"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.lastName}</div>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="email"
                    name="email"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.email}</div>
                </div>
                <div className="col-md-6">
                  <select
                    className="form-control"
                    onChange={this.handleChange}
                    name="department"
                    // defaultValue="Select Department"
                  >
                    <option selected={true} disabled={true}>
                      Select Department
                    </option>
                    <this.Options options={this.state.departments} />
                  </select>
                  <div className="errorMsg">{this.state.errors.department}</div>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="skype id"
                    name="skypeId"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.skypeId}</div>
                </div>
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="contact no"
                    name="contactNumber"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">
                    {this.state.errors.contactNumber}
                  </div>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="experience"
                    name="experience"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.experience}</div>
                </div>
                <div className="col-md-6">
                  <select
                    className="form-control"
                    onChange={this.handleChange}
                    name="role"
                  >
                    <option selected={true} disabled={true}>
                      Select Role
                    </option>
                    <this.Options options={this.state.roles} />
                  </select>
                  <div className="errorMsg">{this.state.errors.role}</div>
                </div>
              </div>
              <br />
              <div className="row">
                <div className="col-md-6">
                  <input
                    className="form-control"
                    onChange={this.handleChange}
                    placeholder="password"
                    name="password"
                    onKeyPress={e => {
                      e.target.keyCode === 13 && e.preventDefault();
                    }}
                  />
                  <div className="errorMsg">{this.state.errors.password}</div>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-md-12 text-center">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.addEmployee.bind(this)}
                  >
                    Save employee
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}
export default Employee;
