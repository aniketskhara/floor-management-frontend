import React, { Component } from "react";
import { Switch, Link, Route, withRouter, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import Modal from "react-modal";
import "../../../assets/styles/admin.scss";
import { appContants } from "../../../config";
import Dashboard from "./dashboard";
import RoleManagement from "./roleManagement";
import DeviceManager from "./deviceManager";
import CabinManager from "./cabinManager";
import Employee from "./employee";
import Master from "./master";
import NotificationAlert from "react-notification-alert";
import axios from "axios";
import { helpers } from "../../../config";

const { routes } = appContants;

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "36%"
  }
};
class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heading: "Dashboard",
      activeRoute: routes[0].title,
      isLoggedIn: true,
      accessRoutes: [],
      masterLink: "/ Cabins",
      updatePass: false,
      currentPassword: "",
      newPassword: "",
      confirmPassword: "",
      type: "password"
    };
  }

  componentDidMount() {
    const { history } = this.props;
    const storageData = localStorage.getItem("access");


    // Check if user is logged in
    if (storageData) {
      const accessArr = storageData.split(",");

      // Adding permissions in routes
      const accessRoutes = routes.filter(item =>
        accessArr.includes(item.value)
      );
      this.setState({ accessRoutes });

      // Check if user has access to following url
      let str;
      str = window.location.href;
      this.authorizeUser(str, accessRoutes, history);

      history.listen(location => {
        const activeRoute = routes.find(
          item => item.route === location.pathname
        );
        if (activeRoute) {
          this.setState({
            heading: activeRoute.title,
            activeRoute: activeRoute.title
          });
        }
      });
    } else {
      history.push("/");
    }
  }

  logout = () => {
    localStorage.clear();
    this.setState({
      isLoggedIn: false
    });
  };

  authorizeUser = (str, accessRoutes, history) => {
    let url;
    if (str) {
      let scrapped_url;
      scrapped_url = str.split("/admin/")[1];
      if (scrapped_url) {
        url = scrapped_url.split("/")[0];
        if (accessRoutes.includes(url)) {
          console.log("permitted");
        } else {
          history.push("/admin");
        }
      }
    }
  };

  NotificationHandler = (message, type) => {
    if (this.refs.notify) {
      this.refs.notify.notificationAlert({
        place: "tr",
        message: message,
        type: type,
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 3
      });
    }
  };

  updateMasterHeader = masterRoute => {
    this.setState({
      masterLink: "/ " + masterRoute
    });
  };

  openModal = () => {
    this.setState({
      updatePass: true
    });
  };

  closeModal = () => {
    this.setState({
      updatePass: false
    });
  };

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  showCurrentPassword = (id) => {
    var x = document.getElementById(id);
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }


  updatePassword = () => {
    if (this.state.newPassword === this.state.confirmPassword){
      var email = localStorage.getItem('user-email');
      this.changePassword(email, this.state.currentPassword, this.state.newPassword);
    }
    else {
      this.NotificationHandler("New Password and Confirm Password did not match", "danger");
    }
  };

  changePassword = (email, password, new_password) => {
    axios
    .post("https://shrouded-springs-70350.herokuapp.com/employees/update-password",          {
        email: email,
        password: password,
        new_password: new_password
      },
    { headers: helpers.buildHeader() })
    .then(response => {
      this.NotificationHandler("Password updated successfully", "success");
      this.closeModal();
    })
    .catch(error => {
      this.NotificationHandler("Unable to update password", "danger");
    });
  }; 


  render() {
    const { heading, activeRoute, accessRoutes, masterLink } = this.state;
    const { match } = this.props;
    const { path } = match;
    if (this.state.isLoggedIn === false) {
      return <Redirect to="/" />;
    } else {
      return (
        <div className="admin-container">
          <NotificationAlert ref="notify" />
          <header className="header">
            <div className="app-name">
              <span className="part-1">Floor</span>
              <span>Manager</span>
            </div>
            <div className="head-container container">
              <div className="col-sm-4 offset-sm-8">
                <div className="row">
                  <div className="col-sm-4 offset-sm-6">
                    <button className="logout100-form-btn" onClick={this.logout}>
                      Logout
                    </button>
                  </div>
                  <div className="col-sm-2">
                    <div className="update-pass">
                      <i class="fa fa-user" aria-hidden="true" onClick={this.openModal}></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <aside className="sidebar">
            <h4 className="heading">Navigation</h4>
            <ul>
              {
                accessRoutes.map((item, index) => (
                  <li className={activeRoute === item.title ? 'active' : ''} key={index}>
                    <i className={item.icon}></i>
                    <Link to={`${item.route}`} className="submenus">{item.title}</Link>
                  </li>
                ))
              }
            </ul>
          </aside>
          <div className="app-content container-fluid">
            <div className="main-header">
              <h2>
                {heading}{" "}
                <span className={heading === "Masters" ? "show" : "hide"}>
                  {" "}
                  {masterLink}
                </span>{" "}
              </h2>
            </div>
            <Switch>
              <Route exact path={`${path}/dashboard`}>
                <Dashboard />
              </Route>
              <Route path={`${path}/role-management`}>
                <RoleManagement />
              </Route>
              <Route path={`${path}/device-manager`}>
                <DeviceManager />
              </Route>
              <Route path={`${path}/cabin-manager`}>
                <CabinManager />
              </Route>
              <Route path={`${path}/employee`}>
                <Employee />
              </Route>
              <Route path={`${path}/master`}>
                <Master triggerUpdateMaster={this.updateMasterHeader} />
              </Route>
            </Switch>
          </div>
          <Modal
            isOpen={this.state.updatePass}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="New Modal"
          >
            <button
              onClick={this.closeModal}
              type="button"
              className="close"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <h3>Update Password</h3>
            <div className="login-form">
              <form action="#">
                <label for="pwd">Current Password:</label>
                <input type="password" class="form-control" id="current-pwd"
                  name="currentPassword" 
                  onChange={this.handleChange}
                  placeholder="Current Password" 
                  onKeyPress={e => {
                    e.target.keyCode === 13 && e.preventDefault();
                  }} />
                <span class="p-viewer" onClick={() => this.showCurrentPassword("current-pwd")}>
                  <i class="fa fa-eye" aria-hidden="true"></i>
                </span>

                <label for="pwd">New Password:</label>
                <input type="password" class="form-control" id="new-pwd" 
                  name="newPassword"
                  onChange={this.handleChange}
                  placeholder="New Password" 
                  onKeyPress={e => {
                    e.target.keyCode === 13 && e.preventDefault();
                  }} />
                <span class="p-viewer" onClick={() => this.showCurrentPassword("new-pwd")}>
                  <i class="fa fa-eye" aria-hidden="true" ></i>
                </span>
                <label for="pwd">Confirm Password:</label>
                <input type="password" class="form-control" id="confirm-pwd" 
                  name="confirmPassword"
                  onChange={this.handleChange}
                  placeholder="Confirm Password" 
                  onKeyPress={e => {
                    e.target.keyCode === 13 && e.preventDefault();
                  }} />
                <span class="p-viewer" onClick={() => this.showCurrentPassword("confirm-pwd")}>
                  <i class="fa fa-eye" aria-hidden="true" ></i>
                </span>
                <button
                  type="button"
                  className="btn btn-secondary update-employee"
                  onClick={this.updatePassword}
                >
                  Update
                </button>
              </form>
            </div>
          </Modal>
        </div>
      );
    }
  }
}

Admin.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired
};
export default withRouter(Admin);
