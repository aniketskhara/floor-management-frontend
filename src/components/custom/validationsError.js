import React, { Component } from "react";

class ValidationError extends Component {
  renderItem = item => {
    return <p>{item}</p>;
  };
  render() {
    const { messages } = this.props;
    return <div>{messages.map(item => this.renderItem(item))}</div>;
  }
}

export default ValidationError;
